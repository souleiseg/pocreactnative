import React from 'react';
import { FlatList, Button, Text, View } from 'react-native';
import {searchMangas} from "../../api/manga-reader-api"

export default class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mangas: []
        };
    }

    _loadMangas() {
        const search = "one";
        console.log(data)
        searchMangas(search).then(data => {
            this.setState({mangas: data})
        })
    }

    render() {
        return (
            <View>
                <Text>Page de recherche de mangas</Text>
                <Button title='Rechercher' onPress={() => this._loadMangas()}/>
                <FlatList
                    data={this.state.mangas}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <Text>{ item.name }</Text>}
                />
            </View>
        )
    }
}