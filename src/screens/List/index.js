import React from "react";
import { View, Text, StyleSheet } from "react-native";

const List = () => {
  return (
    <View style={styles.container}>
      <Text>Page de votre liste de mangas</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default List;
