import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Infos = () => {
  return (
    <View style={styles.container}>
      <Text>Infos</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Infos;
