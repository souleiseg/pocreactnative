import React from 'react';
import { StyleSheet, Text, Button, Alert, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteMood } from "../redux/moods/actions";

const Mood = (props) => {
  const { deleteMood, id, title, mood } = props;
  const bkgColors = ["#9b59b6", "#e74c3c", "#e67e22", "#f1c40f", "#2ecc71"];
  const heights = ["25%", "33%", "50%", "75%", "100%"];

  const _deleteMood = (id) => {
    Alert.alert(
      "Êtes-vous sûr ?",
      "Vous être sur le point de supprimer votre Mood.",
      [
        {
          text: "Annuler",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Supprimer",
          onPress: () => {
            deleteMood(id);
          },
        },
      ],
      { cancelable: false }
    );
    deleteMood(id)
  }

  return (
    <TouchableOpacity
      style={[styles.mood, { backgroundColor: bkgColors[mood - 1], height: heights[mood - 1] }]}
      onLongPress={() => _deleteMood(id)}
    >
      <Text style={styles.moodText}> {title} </Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  mood: {
    width: 80,
    justifyContent: "center",
    flexWrap: "nowrap",
  },
  moodText:{
    textAlign: "center",
    color: "#fff",
    fontWeight: "bold"
  },
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      deleteMood,
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(Mood);
