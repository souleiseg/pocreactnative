import moodsActions from "./constants";
import store from "../../redux/index";

export function addMood(item) {
  const currentStore = store.getState();
  const { moods } = currentStore.moods;


  item.id = Math.max.apply(Math, moods.map((mood) => { return mood.id; }))
  if(!isFinite(item.id)) {
    item.id = 0
  } 
  item.id++;
  
  const newMoods = [
    ...moods,
    item
  ];

  return async function (dispatch) {
    dispatch(setMoods(newMoods));
  }
}

export function deleteMood(itemId) {
  console.log(itemId)

  const currentStore = store.getState();
  const { moods } = currentStore.moods;

  const newMoods = moods.filter(( mood ) => {
    return mood.id !== itemId;
  });

  return async function (dispatch) {
    dispatch(setMoods(newMoods));
  }
}

export function setMoods(moods) {
  // On passe les infos au reducer, via le dispatcher.
  return async function (dispatch) {
    dispatch({
      type: moodsActions.SET_MOODS,
      moods: moods,
    })
  };
}
