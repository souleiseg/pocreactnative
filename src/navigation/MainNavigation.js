import React from "react";
import { StatusBar, Image, Text, StyleSheet, TouchableOpacity, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Home from "../screens/Home"
import List from "../screens/List";
import Favorites from "../screens/Favorites"
import Search from "../screens/Search"
import Profile from "../screens/Profile";

const Stack = createStackNavigator();
const NewStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabsNavigation = () => (
  <Tabs.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeBackgroundColor: "#fff",
      inactiveBackgroundColor: "#fff",
      showLabel: true,
      showIcon: true,
      activeTintColor: "green",
      inactiveTintColor: "#828282",
    }}
    tabStyle={{
      flexDirection: "row",
    }}
  >
    <Tabs.Screen
      name="List"
      component={List}
      listeners={({ navigation }) => ({
        tabPress: () => {
          navigation.navigate("List");
        },
      })}
    />
  </Tabs.Navigator>
);

// Stack Home
const StackHomeNavigation = (props) => {
  const { user } = props;
  const colorStatus = user.loggedIn ? 'green' : 'red';

  return (
    <NewStack.Navigator initialRouteName="Home">
      <NewStack.Screen
        name="Home"
        component={Home}
        options={({ navigation }) => ({
          headerTitle: null,
          headerLeft: () => (
            <View style={[{flexDirection:'row', alignItems:'center'}]}>
            <TouchableOpacity
              onPress={() => navigation.toggleDrawer()}
              style={styles.userHeader}
            >
              <Image
                source={{ uri: "https://img.icons8.com/material/2x/menu.png" }}
                style={styles.logo}
              />
            </TouchableOpacity>
            
              <Text style={styles.userName}>{user.name}</Text>
              <View style={[styles.userStatus, { 'backgroundColor': colorStatus }]} />
            </View>
          ),
          headerRight: () => (
            <Text style={styles.title}>Accueil</Text>
          ),
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </NewStack.Navigator>
  );
}

// Stack Liste
const StackListNavigation = (props) => {
  const { user } = props;
  const colorStatus = user.loggedIn ? 'green' : 'red';

  return (
    <NewStack.Navigator initialRouteName="List">
      <NewStack.Screen
        name="List"
        component={List}
        options={({ navigation }) => ({
          headerTitle: null,
          headerLeft: () => (
            <View style={[{flexDirection:'row', alignItems:'center'}]}>
            <TouchableOpacity
              onPress={() => navigation.toggleDrawer()}
              style={styles.userHeader}
            >
              <Image
                source={{ uri: "https://img.icons8.com/material/2x/menu.png" }}
                style={styles.logo}
              />
            </TouchableOpacity>
            
              <Text style={styles.userName}>{user.name}</Text>
              <View style={[styles.userStatus, { 'backgroundColor': colorStatus }]} />
            </View>
          ),
          headerRight: () => (
            <Text style={styles.title}>Vos mangas</Text>
          ),
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </NewStack.Navigator>
  );
}

// Stack Favoris
const StackFavoritesNavigation = (props) => {
  const { user } = props;
  const colorStatus = user.loggedIn ? 'green' : 'red';

  return (
    <NewStack.Navigator initialRouteName="Favorites">
      <NewStack.Screen
        name="Favorites"
        component={Favorites}
        options={({ navigation }) => ({
          headerTitle: null,
          headerLeft: () => (
            <View style={[{flexDirection:'row', alignItems:'center'}]}>
            <TouchableOpacity
              onPress={() => navigation.toggleDrawer()}
              style={styles.userHeader}
            >
              <Image
                source={{ uri: "https://img.icons8.com/material/2x/menu.png" }}
                style={styles.logo}
              />
            </TouchableOpacity>
            
              <Text style={styles.userName}>{user.name}</Text>
              <View style={[styles.userStatus, { 'backgroundColor': colorStatus }]} />
            </View>
          ),
          headerRight: () => (
            <Text style={styles.title}>Favoris</Text>
          ),
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </NewStack.Navigator>
  );
}

// Stack Recherche
const StackSearchNavigation = (props) => {
  const { user } = props;
  const colorStatus = user.loggedIn ? 'green' : 'red';

  return (
    <NewStack.Navigator initialRouteName="Search">
      <NewStack.Screen
        name="Search"
        component={Search}
        options={({ navigation }) => ({
          headerTitle: null,
          headerLeft: () => (
            <View style={[{flexDirection:'row', alignItems:'center'}]}>
            <TouchableOpacity
              onPress={() => navigation.toggleDrawer()}
              style={styles.userHeader}
            >
              <Image
                source={{ uri: "https://img.icons8.com/material/2x/menu.png" }}
                style={styles.logo}
              />
            </TouchableOpacity>
            
              <Text style={styles.userName}>{user.name}</Text>
              <View style={[styles.userStatus, { 'backgroundColor': colorStatus }]} />
            </View>
          ),
          headerRight: () => (
            <Text style={styles.title}>Rechercher</Text>
          ),
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </NewStack.Navigator>
  );
}

// Stack Profile
const StackProfileNavigation = (props) => {
  const { user } = props;
  const colorStatus = user.loggedIn ? 'green' : 'red';

  return (
    <NewStack.Navigator initialRouteName="Profile">
      <NewStack.Screen
        name="Profile"
        component={Profile}
        options={({ navigation }) => ({
          headerTitle: null,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.toggleDrawer()}
              style={styles.userHeader}
            >
              <Image
                source={{ uri: "https://img.icons8.com/material/2x/menu.png" }}
                style={styles.logo}
              />
              <Text style={styles.userName}>{user.name}</Text>
              <View style={[styles.userStatus, { 'backgroundColor': colorStatus }]} />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <Text style={styles.title}>Profile</Text>
          ),
          headerStyle: {
            backgroundColor: '#fff',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        })}
      />
    </NewStack.Navigator>
  );
}

const MainNavigation = (props) => {
  const { user } = props;

  return (
    <NavigationContainer>
      <StatusBar
        hidden={false}
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Accueil">
          {props => <StackHomeNavigation {...props} user={user} />}
        </Drawer.Screen>
        <Drawer.Screen name="Vos mangas">
          {props => <StackListNavigation {...props} user={user} />}
        </Drawer.Screen>
        <Stack.Screen name="Favoris">
          {props => <StackFavoritesNavigation {...props} user={user} />}
        </Stack.Screen>
        <Stack.Screen name="Rechercher">
          {props => <StackSearchNavigation {...props} user={user} />}
        </Stack.Screen>
        <Stack.Screen name="Profile">
          {props => <StackProfileNavigation {...props} user={user} />}
        </Stack.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: "700",
    marginRight: 20,
  },
  logo: {
    marginLeft: 20,
    width: 28,
    height: 28,
  },
  userHeader: {
    flexDirection: "row",
    alignItems: "center",
  },
  userName: {
    fontWeight: "600",
    marginLeft: 12,
  },
  userStatus: {
    width: 8,
    height: 8,
    borderRadius: 50,
    marginLeft: 4,
    marginTop: -14,
  }
});

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(MainNavigation);
